#!/opt/zabbix_rabbit_send/env/bin/python3
# -*- coding: utf-8 -*-
import sys
import json
import time
import logging
import sys, getopt
import pika
from flask import Flask, request, abort
from flask_httpauth import HTTPBasicAuth
from config import USERNAME,PASSWORD,HOST,PORT,QUEUE,EXCHANGE
auth = HTTPBasicAuth()
app = Flask(__name__)
def send_rabbit(message):
    exchange=EXCHANGE
    queue=QUEUE
    credentials=credentials=pika.credentials.PlainCredentials(username=USERNAME, password=PASSWORD, erase_on_connect=False)
    connection = pika.BlockingConnection(pika.ConnectionParameters(
             host=HOST,port=PORT, credentials=credentials))
    channel = connection.channel()
    #channel.queue_declare(queue=queue)
    channel.basic_publish(exchange=exchange,
                          #routing_key=queue,
                          routing_key='',
                          body=message)
    connection.close()

def convert_tags_format(tags):
    res={}
    if len(tags)>0:
        vlist=[]
        for t in tags.split(", "):
            if t.split(":")[0] != 'host_id':
                if t.split(":")[0] in res.keys():
                    if type(res[t.split(":")[0]]) == str:
                        vlist.append(res.pop(t.split(":")[0]))
                        vlist.append(t.split(":")[1])
                        res.update({t.split(":")[0]:vlist})
                        vlist=[]
                    elif type(res[t.split(":")[0]]) == list:
                        l = res.pop(t.split(":")[0])
                        for ll in l:
                            vlist.append(ll)
                        vlist.append(t.split(":")[1])
                        res.update({t.split(":")[0]:vlist})
                else:
                    if ":" in t:
                        res.update({t.split(":")[0]:t.split(":")[1]})
                    else:
                        res.update({t:None})
    return res

def get_host_id(tags):
    res=''
    if len(tags)>0:
        for t in tags.split(", "):
            if t.split(":")[0] == 'host_id':
                res=t.split(":")[1]
    return res

@auth.get_password
def get_password(username):
    if username == USERNAME:
        return PASSWORD
    return None

@auth.error_handler
def unauthorized():
    return abort(401)

@app.route("/send", methods=['POST'])
@auth.login_required
def send():
    message=eval(request.data.decode("utf-8"))
    ctime=message['properties']['startTime']
    message['properties']['startTime'] = time.strftime("%Y-%m-%dT%H:%M:%SZ",time.strptime(ctime,"%Y.%m.%d %H:%M:%S"))
    message['properties']['timestamp'] = str(int(time.mktime(time.strptime(ctime,"%Y.%m.%d %H:%M:%S"))))
    trigger_tags=message['properties']['triggerTags']
    message['properties']['triggerTags'] = convert_tags_format(trigger_tags)
    message['properties']['zabbixHostId'] = get_host_id(trigger_tags)
    send_rabbit(json.dumps(message))
    return '200'

if __name__ == "__main__":
    app.run(host='0.0.0.0')